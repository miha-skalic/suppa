# -*- coding: utf-8 -*-
"""
Created on Wed Aug 06 17:51:05 2014

@author: Gael P Alamancos
@email: gael.perez[at]upf.edu
"""


import psiPerGene as psiPerIsoform
import psiCalculator as psiPerEvent
import eventGenerator as generateEvents
import logging
import argparse 
import sys


description = "Description:\n\n" + \
              "This program allows you to generate all the possible Alternative Splicing events\n" + \
              "from an annotation file(gtf) and then calculate the PSI(Percentatge of Spliced In\n" + \
              "per event and isoform an expression file.\n" + \
              "For further information, see the help of each subcommand."

parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
subparsers = parser.add_subparsers()

#EventGenerator parser
eventGeneratorSubparser = subparsers.add_parser(
    "generateEvents", parents=[generateEvents.parser],
    help="Calculates the different Alternative Splicing events based on a gtf file.")
eventGeneratorSubparser.set_defaults(which="generateEvents")

#psiCalculator parser
psiCalculatorSubparser = subparsers.add_parser(
    "psiPerEvent", parents=[psiPerEvent.parser],
    help="Calculates the PSI value per each event previously generated.")
psiCalculatorSubparser.set_defaults(which="psiPerEvent")

#psiPerGene parser
psiPerGeneSubparser = subparsers.add_parser(
    "psiPerIsoform", parents=[psiPerIsoform.parser],
    help="Calculates the PSI value per each isoform.")
psiPerGeneSubparser.set_defaults(which="psiPerIsoform")

#Setting logging preferences
logger = logging.getLogger(__name__)


def main():
    try:
        args = parser.parse_args()
        if args.which == "generateEvents":
            generateEvents.parser = parser  # Setting the module aparser
            generateEvents.main()
        elif args.which == "psiPerEvent":
            psiPerEvent.parser = parser  # Setting the module parser
            psiPerEvent.main()
        elif args.which == "psiPerIsoform":
            psiPerIsoform.parser = parser  # Setting the module parser
            psiPerIsoform.main()
    except Exception:
        logger.error("Unknown error: %s" % sys.exc_info()[1].args[0])
        sys.exit(1)
        
if __name__ == '__main__':
    main()
