import itertools
from lib.event import Event


class VariableEvent(Event):
    """
    PCR2 events - events with thresholds
    """

    def merge_events(self, threshold, overlap_fun):
        """
        Adds new transcripts to existing. overlap_fun
        evals if two events overlap
        """
        ids = {event: [] for event in self.positive_ids}
        # ids = {event: [event] for event in self.positive_ids}
        # append 'subevents'
        for id1, id2 in itertools.combinations(self.positive_ids, 2):
            if overlap_fun(id1, id2, threshold):
                ids[id1] += [id2]
                ids[id2] += [id1]

        # add new events
        for id1 in ids:
            for id2 in ids[id1]:
                self.positive_ids[id1] += self.positive_ids[id2]
                self.negative_ids[id1] += self.negative_ids[id2]

        for idx in self.positive_ids:
            self.positive_ids[idx] = list(set(self.positive_ids[idx]))
            self.negative_ids[idx] = list(set(self.negative_ids[idx]))


class SEpcr(Event):
    """
    Skipped exon event - only full match coordinates!
    """
    def __init__(self, gene, *_):
        Event.__init__(self, gene)
        self.etype = 'SEpcr'

        self.search_ids = set([])

        self.construct_events(gene)

    def add_putative_events(self, exons, transcript):
        if len(exons) < 3:
            return
        for i in range(len(exons) - 1):
            firstexon = exons[i]
            secondexon = exons[i + 1]
            coords = (firstexon[1], secondexon[0])
            self.alt_search[coords] = self.alt_search.get(coords, []) + [transcript]

    def add_real_events(self, exons, transcript):
        gene = self.gene
        if len(exons) < 2:
            return
        for i in range(1, len(exons) - 1):
            midexon = exons[i]

            for coord1, coord2 in self.alt_search:
                if coord1 < midexon[0] and midexon[1] < coord2:
                    eventid = '{}:{}:{}:{}'.format(gene.chr, midexon[0], midexon[1], gene.strand)
                    self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
                    self.negative_ids[eventid] = self.negative_ids.get((coord1, coord2), []) + \
                        self.alt_search[(coord1, coord2)]

    def export_events_gtf(self, _):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line4 = self.gtf_string.format(e_vals[2], e_vals[3], strand, full_event, full_event, 'alternative1')
            yield line4, self.etype


class SEpcr2(VariableEvent):
    def __init__(self, gene, threshold=10):
        Event.__init__(self, gene)
        self.etype = 'SEpcr2'

        self.construct_events(gene)
        self.clear_redundant()

        if len(self.positive_ids) > 1 and threshold:
            self.merge_events(threshold=threshold, overlap_fun=self.overlap)

    def add_putative_events(self, exons, transcript):
        if len(exons) < 3:
            return
        gene = self.gene
        for i in range(len(exons) - 2):
            firstexon = exons[i]
            midexon = exons[i + 1]
            lastexon = exons[i + 2]
            eventid = '{}:{}-{}:{}-{}:{}'.format(gene.chr, firstexon[1], midexon[0],
                                                 midexon[1], lastexon[0], gene.strand)

            self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
            search_coord = (firstexon[1], lastexon[0])
            search = self.alt_search.get(search_coord, set())
            search.add(eventid)
            self.alt_search[search_coord] = search

    def add_real_events(self, exons, transcript):
        if len(exons) < 2:
            return
        for i in range((len(exons)-1)):
            firstexon = exons[i]
            secondexon = exons[i + 1]
            search_coord = (firstexon[1], secondexon[0])

            if search_coord in self.alt_search:
                eventids = self.alt_search[search_coord]
                for eventid in eventids:
                    self.negative_ids[eventid] = self.negative_ids.get(eventid, []) + [transcript]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative2')
            yield line1, self.etype

            line2 = self.gtf_string.format(e_vals[5], int(e_vals[5]) + edge, strand, full_event, full_event,
                                           'alternative2')
            yield line2, self.etype

            line3 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative1')
            yield line3, self.etype

            line4 = self.gtf_string.format(e_vals[3], e_vals[4], strand, full_event, full_event, 'alternative1')
            yield line4, self.etype

            line5 = self.gtf_string.format(e_vals[5], int(e_vals[5]) + edge, strand, full_event, full_event,
                                           'alternative1')
            yield line5, self.etype

    def clear_redundant(self):
        for id_key in list(self.positive_ids.keys()):
            if id_key not in self.negative_ids:
                self.positive_ids.pop(id_key)

    @staticmethod
    def overlap(id1, id2, th):
        """
        Returns true if all positions overlap within a certain threshold
        """
        first = [int(pos) for pos in id1[:-2].replace('-', ':').split(':')[1:]]
        second = [int(pos) for pos in id2[:-2].replace('-', ':').split(':')[1:]]
        if all(map(lambda x: abs(x[0] - x[1]) < th, zip(first, second))):
            return True
        else:
            return False


class RIpcr(VariableEvent):
    """
    Retained intro event. PCR version
    """
    def __init__(self, gene, *_):
        Event.__init__(self, gene)
        self.etype = 'RIpcr'

        self.construct_events(gene)

    def add_putative_events(self, exons, transcript):
        gene = self.gene
        for i in range(len(exons) - 1):
            firstexon = exons[i]
            secondexon = exons[i + 1]
            eventid = '{}:{}-{}:{}'.format(gene.chr, firstexon[1], secondexon[0], gene.strand)

            self.negative_ids[eventid] = self.negative_ids.get(eventid, []) + [transcript]
            search_coord = (firstexon[1], secondexon[0])
            search = self.alt_search.get(search_coord, set())
            search.add(eventid)
            self.alt_search[search_coord] = search

    def add_real_events(self, exons, transcript):
        for i in range(len(exons)):
            search_coord = exons[i]

            for stash_pair in self.alt_search:
                if (search_coord[0] < stash_pair[0]) and (search_coord[1] > stash_pair[1]):
                    eventids = self.alt_search[stash_pair]
                    for eventid in eventids:
                        self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """

        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)

            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event,
                                           full_event, 'alternative2')
            yield line1, self.etype

            line2 = self.gtf_string.format(e_vals[3], int(e_vals[3]) + edge, strand, full_event,
                                           full_event, 'alternative2')
            yield line2, self.etype

            line3 = self.gtf_string.format(e_vals[2], e_vals[3], strand, full_event, full_event, 'alternative1')
            yield line3, self.etype


class RIpcr2(VariableEvent):
    """
    Retained intro event. PCR version with threshold
    """
    def __init__(self, gene, threshold):
        Event.__init__(self, gene)
        self.etype = 'RIpcr2'

        self.construct_events(gene)

        if len(self.positive_ids) > 1 and threshold:
            self.merge_events(threshold=threshold, overlap_fun=self.overlap)

    def add_putative_events(self, exons, transcript):
        gene = self.gene
        for i in range(len(exons) - 1):
            firstexon = exons[i]
            secondexon = exons[i + 1]
            eventid = '{}:{}-{}:{}'.format(gene.chr, firstexon[1], secondexon[0], gene.strand)

            self.negative_ids[eventid] = self.negative_ids.get(eventid, []) + [transcript]
            search_coord = (firstexon[1], secondexon[0])
            search = self.alt_search.get(search_coord, set())
            search.add(eventid)
            self.alt_search[search_coord] = search

    def add_real_events(self, exons, transcript):
        for i in range(len(exons)):
            search_coord = exons[i]

            for stash_pair in self.alt_search:
                if (search_coord[0] < stash_pair[0]) and (search_coord[1] > stash_pair[1]):
                    eventids = self.alt_search[stash_pair]
                    for eventid in eventids:
                        self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """

        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)

            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand,
                                           full_event, full_event, 'alternative2')
            yield line1, self.etype

            line2 = self.gtf_string.format(e_vals[3], int(e_vals[3]) + edge, strand,
                                           full_event, full_event, 'alternative2')
            yield line2, self.etype

            line3 = self.gtf_string.format(e_vals[2], e_vals[3], strand, full_event,
                                           full_event, 'alternative1')
            yield line3, self.etype

    @staticmethod
    def overlap(id1, id2, th):
        """
        Returns true if all positions overlap within a certain threshold
        """

        first = [int(pos) for pos in id1[:-2].replace('-', ':').split(':')[1:]]
        second = [int(pos) for pos in id2[:-2].replace('-', ':').split(':')[1:]]

        if all(map(lambda x: abs(x[0] - x[1]) < th, zip(first, second))):
            return True
        else:
            return False


class ARSSpcr(Event):
    """
    Alternative 'Right' Splice Site. PCR style.
    """
    def __init__(self, gene, *_):
        Event.__init__(self, gene)
        if self.gene.strand == '-':
            self.etype = 'A3pcr'
        else:
            self.etype = 'A5pcr'
        self.positive_coords = {}
        self.negative_coords = {}

        self.construct_events(gene)

    def add_putative_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            search_coords = firstexon[1]
            self.negative_coords[search_coords] = self.negative_coords.get(search_coords, []) + [transcript]

    def add_real_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            self.create_arss(firstexon[0], firstexon[1], transcript)

    def create_arss(self, coord1, coord2, transcript):
        gene = self.gene
        for neg1 in self.negative_coords:
            if coord1 < neg1 < coord2:
                eventid = '{}:{}:{}:{}'.format(gene.chr, neg1, coord2,  gene.strand)
                self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
                self.negative_ids[eventid] = self.negative_coords[neg1]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[3], strand, full_event, full_event,
                                           'alternative1')
            yield line1, self.etype

            line3 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative2')
            yield line3, self.etype


class ALSSpcr(Event):
    """
    Alternative 'Left' Splice Site. PCR style.
    """
    def __init__(self, gene, *_):
        Event.__init__(self, gene)
        if self.gene.strand == '-':
            self.etype = 'A5pcr'
        else:
            self.etype = 'A3pcr'
        self.positive_coords = {}
        self.negative_coords = {}

        self.construct_events(gene)

    def add_putative_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            search_coords = firstexon[0]
            self.negative_coords[search_coords] = self.negative_coords.get(search_coords, []) + [transcript]

    def add_real_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            self.create_alss(firstexon[0], firstexon[1], transcript)

    def create_alss(self, coord1, coord2, transcript):
        gene = self.gene
        for neg1 in self.negative_coords:
            if coord1 < neg1 < coord2:
                eventid = '{}:{}:{}:{}'.format(gene.chr, coord1, neg1, gene.strand)
                self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
                self.negative_ids[eventid] = self.negative_coords[neg1]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]), int(e_vals[3]) + edge, strand, full_event, full_event,
                                           'alternative1')
            yield line1, self.etype

            line3 = self.gtf_string.format(e_vals[2], int(e_vals[3]) + edge, strand, full_event, full_event,
                                           'alternative2')
            yield line3, self.etype


class ARSSpcr2(VariableEvent):
    """
    Alternative 'Right' Splice Site. PCR style.
    """
    def __init__(self, gene, threshold):
        Event.__init__(self, gene)
        if self.gene.strand == '-':
            self.etype = 'A3pcr2'
        else:
            self.etype = 'A5pcr2'
        self.positive_coords = {}
        self.negative_coords = {}

        self.construct_events(gene)

        if len(self.positive_ids) > 1 and threshold:
            self.merge_events(threshold=threshold, overlap_fun=self.overlap)

    def add_putative_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            search_coords = firstexon[1]
            self.negative_coords[search_coords] = self.negative_coords.get(search_coords, []) + [transcript]

    def add_real_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            self.create_arss(firstexon[0], firstexon[1], transcript)

    def create_arss(self, coord1, coord2, transcript):
        gene = self.gene
        for neg1 in self.negative_coords:
            if coord1 < neg1 < coord2:
                eventid = '{}:{}:{}:{}'.format(gene.chr, neg1, coord2,  gene.strand)
                self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
                self.negative_ids[eventid] = self.negative_coords[neg1]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[3], strand, full_event, full_event,
                                           'alternative1')
            yield line1, self.etype

            line3 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative2')
            yield line3, self.etype

    @staticmethod
    def overlap(id1, id2, th):
        """
        Returns true if all positions overlap within a certain threshold
        """

        first = [int(pos) for pos in id1[:-2].replace('-', ':').split(':')[1:]]
        second = [int(pos) for pos in id2[:-2].replace('-', ':').split(':')[1:]]
        if first[0] == second[0] and abs(first[1] - second[0]) < th:
            return True
        else:
            return False


class ALSSpcr2(VariableEvent):
    """
    Alternative 'Left' Splice Site. PCR style.
    """
    def __init__(self, gene, threshold):
        Event.__init__(self, gene)
        if self.gene.strand == '-':
            self.etype = 'A5pcr2'
        else:
            self.etype = 'A3pcr2'
        self.positive_coords = {}
        self.negative_coords = {}

        self.construct_events(gene)

        if len(self.positive_ids) > 1 and threshold:
            self.merge_events(threshold=threshold, overlap_fun=self.overlap)

    def add_putative_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            search_coords = firstexon[0]
            self.negative_coords[search_coords] = self.negative_coords.get(search_coords, []) + [transcript]

    def add_real_events(self, exons, transcript):
        for i in range(len(exons)):
            firstexon = exons[i]
            self.create_alss(firstexon[0], firstexon[1], transcript)

    def create_alss(self, coord1, coord2, transcript):
        gene = self.gene
        for neg1 in self.negative_coords:
            if coord1 < neg1 < coord2:
                eventid = '{}:{}:{}:{}'.format(gene.chr, coord1, neg1, gene.strand)
                self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
                self.negative_ids[eventid] = self.negative_coords[neg1]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]), int(e_vals[3]) + edge, strand, full_event, full_event,
                                           'alternative1')
            yield line1, self.etype

            line3 = self.gtf_string.format(e_vals[2], int(e_vals[3]) + edge, strand, full_event, full_event,
                                           'alternative2')
            yield line3, self.etype

    @staticmethod
    def overlap(id1, id2, th):
        """
        Returns true if all positions overlap within a certain threshold
        """
        first = [int(pos) for pos in id1[:-2].replace('-', ':').split(':')[1:]]
        second = [int(pos) for pos in id2[:-2].replace('-', ':').split(':')[1:]]
        if first[1] == second[1] and abs(first[0] - second[0]) < th:
            return True
        else:
            return False


class SEpcr3(VariableEvent):
    def __init__(self, gene, threshold=10):
        Event.__init__(self, gene)
        self.etype = 'SEpcr3'

        self.construct_events(gene)
        self.clear_redundant()

        if len(self.positive_ids) > 1 and threshold:
            self.merge_events(threshold=threshold, overlap_fun=self.overlap)

    def add_putative_events(self, exons, transcript):
        if len(exons) < 3:
            return
        gene = self.gene
        for i in range(len(exons) - 2):
            firstexon = exons[i]
            midexon = exons[i + 1]
            lastexon = exons[i + 2]
            eventid = '{}:{}-{}:{}-{}:{}'.format(gene.chr, firstexon[1], midexon[0],
                                                 midexon[1], lastexon[0], gene.strand)

            self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
            search_coord = (firstexon[1], lastexon[0])
            search = self.alt_search.get(search_coord, set())
            search.add(eventid)
            self.alt_search[search_coord] = search

    def add_real_events(self, exons, transcript):
        if len(exons) < 2:
            return
        for i in range((len(exons)-1)):
            firstexon = exons[i]
            secondexon = exons[i + 1]
            search_coord = (firstexon[1], secondexon[0])

            if search_coord in self.alt_search:
                eventids = self.alt_search[search_coord]
                for eventid in eventids:
                    self.negative_ids[eventid] = self.negative_ids.get(eventid, []) + [transcript]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative2')
            yield line1, self.etype

            line2 = self.gtf_string.format(e_vals[5], int(e_vals[5]) + edge, strand, full_event, full_event,
                                           'alternative2')
            yield line2, self.etype

            line3 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative1')
            yield line3, self.etype

            line4 = self.gtf_string.format(e_vals[3], e_vals[4], strand, full_event, full_event, 'alternative1')
            yield line4, self.etype

            line5 = self.gtf_string.format(e_vals[5], int(e_vals[5]) + edge, strand, full_event, full_event,
                                           'alternative1')
            yield line5, self.etype

    def clear_redundant(self):
        for id_key in list(self.positive_ids.keys()):
            if id_key not in self.negative_ids:
                self.positive_ids.pop(id_key)

    @staticmethod
    def overlap(id1, id2, th):
        """
        Returns true if all positions overlap within a certain threshold
        """
        first = [int(pos) for pos in id1[:-2].replace('-', ':').split(':')[1:]]
        second = [int(pos) for pos in id2[:-2].replace('-', ':').split(':')[1:]]
        #if all(map(lambda x: abs(x[0] - x[1]) < th, zip(first, second))):
        if first[0] == second[0] \
                and first[3] == second[3] \
                and abs(first[1] - second[1]) < th \
                and abs(first[2] - second[2]) < th:
            return True
        return False


class SEv(VariableEvent):
    def __init__(self, gene, threshold=10):
        Event.__init__(self, gene)
        self.etype = 'SE'

        self.construct_events(gene)
        self.clear_redundant()

        if len(self.positive_ids) > 1 and threshold:
            self.merge_events(threshold=threshold, overlap_fun=self.overlap)

    def add_putative_events(self, exons, transcript):
        if len(exons) < 3:
            return
        gene = self.gene
        for i in range(len(exons) - 2):
            firstexon = exons[i]
            midexon = exons[i + 1]
            lastexon = exons[i + 2]
            eventid = '{}:{}-{}:{}-{}:{}'.format(gene.chr, firstexon[1], midexon[0],
                                                 midexon[1], lastexon[0], gene.strand)

            self.positive_ids[eventid] = self.positive_ids.get(eventid, []) + [transcript]
            search_coord = (firstexon[1], lastexon[0])
            search = self.alt_search.get(search_coord, set())
            search.add(eventid)
            self.alt_search[search_coord] = search

    def add_real_events(self, exons, transcript):
        if len(exons) < 2:
            return
        for i in range((len(exons)-1)):
            firstexon = exons[i]
            secondexon = exons[i + 1]
            search_coord = (firstexon[1], secondexon[0])

            if search_coord in self.alt_search:
                eventids = self.alt_search[search_coord]
                for eventid in eventids:
                    self.negative_ids[eventid] = self.negative_ids.get(eventid, []) + [transcript]

    def export_events_gtf(self, edge):
        """
        Generator of GTF lines
        """
        strand = self.gene.strand
        for event in self.positive_ids:
            full_event = '{}:{}'.format(self.etype, event)
            e_vals = full_event.replace('-', ':').split(':')

            line1 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative2')
            yield line1, self.etype

            line2 = self.gtf_string.format(e_vals[5], int(e_vals[5]) + edge, strand, full_event, full_event,
                                           'alternative2')
            yield line2, self.etype

            line3 = self.gtf_string.format(int(e_vals[2]) - edge, e_vals[2], strand, full_event, full_event,
                                           'alternative1')
            yield line3, self.etype

            line4 = self.gtf_string.format(e_vals[3], e_vals[4], strand, full_event, full_event, 'alternative1')
            yield line4, self.etype

            line5 = self.gtf_string.format(e_vals[5], int(e_vals[5]) + edge, strand, full_event, full_event,
                                           'alternative1')
            yield line5, self.etype

    def clear_redundant(self):
        for id_key in list(self.positive_ids.keys()):
            if id_key not in self.negative_ids:
                self.positive_ids.pop(id_key)

    @staticmethod
    def overlap(id1, id2, th):
        """
        Returns true if all positions overlap within a certain threshold
        """
        first = [int(pos) for pos in id1[:-2].replace('-', ':').split(':')[1:]]
        second = [int(pos) for pos in id2[:-2].replace('-', ':').split(':')[1:]]
        if all([abs(first[0] - second[0]) < th,
                first[1] == second[1],
                first[2] == second[2],
                abs(first[3] - second[3]) < th]):
            print(id1)
            return True
        else:
            return False
